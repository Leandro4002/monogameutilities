﻿namespace MonoGameUtilities {
    /**
     * Exemple of delay using this class :
     * 
     * jumpDelay = new Delay(1);
     * 
     * Update(GameTime gameTime) {
     *   jumpDelay.Update(gameTime.ElapsedGameTime.TotalSeconds);
     *   
     *   if (keyDown("W") && jumpDelay.isTrigger) {
     *     //Make the player jump
     *     jumpDelay.Reset();
     *   }
     * }
     * 
    **/
    public class Delay {
        public float handler, timer;
        public bool isTrigger;

        public Delay(float timer, bool initializeByDefault = true) {
            this.timer = timer;
            if (initializeByDefault) {
                handler = timer;
            }
        }

        public void Update(float dt) {
            if (!isTrigger) {
                if (handler - dt < 0) {
                    handler = 0;
                    isTrigger = true;
                } else {
                    handler -= dt;
                }
            }
        }

        public void Reset() {
            handler = timer;
            isTrigger = false;
        }
    }
}
