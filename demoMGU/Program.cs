﻿using System;

namespace DemoMGU
{
    public static class Program
    {
        public static DemoMGU game;

        [STAThread]
        static void Main()
        {
            game = new DemoMGU();
            game.Run();
        }
    }
}
