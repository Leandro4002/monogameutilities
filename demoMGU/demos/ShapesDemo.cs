﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;
using System;

namespace DemoMGU
{
    class ShapesDemo : Demo
    {
        Vector2 textPosition, shapePosition;
        Texture2D rectTexture, circleTexture;
        int rectSize = 100, circleRadius = 50;
        bool isColliding = false;
        Scenario currentScenario;
        MouseState mouseState, oldMouseState;
        int numOfScenarii;

        enum Scenario {
            Circle2Circle,
            Rect2Rect,
            Circle2Rect,
            Rect2Circle,
            Pos2Circle,
            Pos2Rect
        }

        public ShapesDemo()
        {
            textPosition = new Vector2(50, 200);
            shapePosition = new Vector2(550, 250);
            rectTexture = Tools.CreateRectTexture(Program.game.GraphicsDevice, rectSize, rectSize, Color.White);
            circleTexture = Tools.CreateCircleTexture(Program.game.GraphicsDevice, circleRadius, Color.White);
            currentScenario = Scenario.Circle2Circle;
            numOfScenarii = Enum.GetNames(typeof(Scenario)).Length;
        }

        public override void Update(GameTime gameTime)
        {
            mouseState = Mouse.GetState();

            if (mouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
            {
                currentScenario++;
                if ((int)currentScenario > numOfScenarii - 1) currentScenario = (Scenario)0;
            }

            oldMouseState = mouseState;

            switch (currentScenario)
            {
                case Scenario.Circle2Circle: isColliding = Tools.Circle2Circle(mouseState.X, mouseState.Y, circleRadius, shapePosition.X, shapePosition.Y, circleRadius); break;
                case Scenario.Rect2Rect: isColliding = Tools.Rect2Rect(mouseState.X, mouseState.Y, rectSize, rectSize, shapePosition.X, shapePosition.Y, rectSize, rectSize); break;
                case Scenario.Circle2Rect: isColliding = Tools.Circle2Rect(mouseState.X, mouseState.Y, circleRadius, shapePosition.X, shapePosition.Y, rectSize, rectSize); break;
                case Scenario.Rect2Circle: isColliding = Tools.Circle2Rect(shapePosition.X, shapePosition.Y, circleRadius, mouseState.X, mouseState.Y, rectSize, rectSize); break;
                case Scenario.Pos2Circle: isColliding = Tools.Pos2Circle(mouseState.X, mouseState.Y, shapePosition.X, shapePosition.Y, circleRadius); break;
                case Scenario.Pos2Rect: isColliding = Tools.Pos2Rect(mouseState.X, mouseState.Y, shapePosition.X, shapePosition.Y, rectSize, rectSize); break;
            }
        }

        public override void Draw()
        {
            Color shapeColor = Color.Red, mouseShapeColor = Color.Green;
            if (isColliding) { shapeColor = Color.Yellow; mouseShapeColor = Color.Yellow; }

            switch (currentScenario)
            {
                case Scenario.Circle2Circle:
                    Program.game.SpriteBatch.Draw(circleTexture, mouseState.Position.ToVector2() - new Vector2(circleRadius), mouseShapeColor);
                    Program.game.SpriteBatch.Draw(circleTexture, shapePosition - new Vector2(circleRadius), shapeColor);
                    break;
                case Scenario.Rect2Rect:
                    Program.game.SpriteBatch.Draw(rectTexture, mouseState.Position.ToVector2(), mouseShapeColor);
                    Program.game.SpriteBatch.Draw(rectTexture, shapePosition, shapeColor);
                    break;
                case Scenario.Circle2Rect:
                    Program.game.SpriteBatch.Draw(circleTexture, mouseState.Position.ToVector2() - new Vector2(circleRadius), mouseShapeColor);
                    Program.game.SpriteBatch.Draw(rectTexture, shapePosition, shapeColor);
                    break;
                case Scenario.Rect2Circle:
                    Program.game.SpriteBatch.Draw(rectTexture, mouseState.Position.ToVector2(), mouseShapeColor);
                    Program.game.SpriteBatch.Draw(circleTexture, shapePosition - new Vector2(circleRadius), shapeColor);
                    break;
                case Scenario.Pos2Circle:
                    Program.game.SpriteBatch.Draw(circleTexture, shapePosition - new Vector2(circleRadius), shapeColor);
                    break;
                case Scenario.Pos2Rect:
                    Program.game.SpriteBatch.Draw(rectTexture, shapePosition, shapeColor);
                    break;
            }

            string text = "Move your mouse to play with the collisions\n" +
                "Left click to cycle to different scenario\n" +
                "currentScenario: " + currentScenario + "\n" + 
                "isColliding: " + isColliding;
            Program.game.SpriteBatch.DrawString(Program.game.consolasFont, text, textPosition, Color.Black);
        }
    }
}
