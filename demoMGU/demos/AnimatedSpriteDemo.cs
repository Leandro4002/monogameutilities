﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace DemoMGU
{
    class AnimatedSpriteDemo : Demo
    {
        AnimatedSprite explosionAnimation, runAnimation;
        Vector2 explositionAnimationPos, runAnimationPos;

        public AnimatedSpriteDemo()
        {
            explositionAnimationPos = new Vector2(200, 150);
            runAnimationPos = new Vector2(400, 150);
            Texture2D explosionTexture = Program.game.Content.Load<Texture2D>("sprites/animations/explosion1_f17w64h64c6r3");
            Texture2D runTexture = Program.game.Content.Load<Texture2D>("sprites/animations/playerRun_f8w32h32c8r1");

            explosionAnimation = new AnimatedSprite(explosionTexture, 15).animParam(isLooping: true);
            runAnimation = new AnimatedSprite(runTexture, 10).animParam(isLooping: true);
            runAnimation.scale = new Vector2(5);
        }

        public override void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;
            explosionAnimation.Update(dt);
            runAnimation.Update(dt);
        }

        public override void Draw()
        {
            Program.game.SpriteBatch.DrawAnim(explosionAnimation, explositionAnimationPos);
            Program.game.SpriteBatch.DrawAnim(runAnimation, runAnimationPos);
        }
    }
}
