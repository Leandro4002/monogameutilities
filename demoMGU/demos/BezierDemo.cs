﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace DemoMGU
{
    class BezierDemo : Demo
    {
        Vector2[] points;
        Texture2D quadraticCurve, cubicCurve;
        Vector2 quadraticCurvePos, cubicCurvePos;
        int curveTextureSize = 200, curvePosition = 300;

        public BezierDemo()
        {
            quadraticCurvePos = new Vector2(100, 100);
            points = new Vector2[] { new Vector2(0, 0), new Vector2(.7f, .2f), new Vector2(1, 1) };
            quadraticCurve = QuadraticBezier.GenerateCurve(Program.game.GraphicsDevice, curveTextureSize, curvePosition, Color.Black, points, false);

            cubicCurvePos = new Vector2(400, 100);
            points = new Vector2[] { new Vector2(0, 0), new Vector2(.25f, 1f), new Vector2(.75f, 0), new Vector2(1, 1) };
            cubicCurve = CubicBezier.GenerateCurve(Program.game.GraphicsDevice, curveTextureSize, curvePosition, Color.Red, points, true);
        }

        public override void Update(GameTime gameTime)
        {
            points[1].Y = (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds) / 2 + 0.5f;
            points[2].Y = (float)Math.Cos(gameTime.TotalGameTime.TotalSeconds) / 2 + 0.5f;
            cubicCurve = CubicBezier.GenerateCurve(Program.game.GraphicsDevice, curveTextureSize, curvePosition, Color.Red, points, true);
        }

        public override void Draw()
        {
            Program.game.SpriteBatch.Draw(cubicCurve, cubicCurvePos, Color.White);
            Program.game.SpriteBatch.Draw(quadraticCurve, quadraticCurvePos, Color.White);

            for (int i = 0; i < points.Length; i++) {
                Program.game.SpriteBatch.DrawRectangle(
                    new Rectangle(new Point((int)cubicCurvePos.X, (int)cubicCurvePos.Y) + new Point((int)(points[i].X * curveTextureSize) - 2, (int)(points[i].Y * curveTextureSize) - 2),
                    new Point(4, 4)), Color.Green);
            }
        }
    }
}
