﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace DemoMGU
{
    class ParticlesSystemDemo : Demo
    {
        DemoParticles.SmokeParticleSystem smokeParticleSystem;
        KeyboardState kState, oldKState;
        Vector2 textPosition;

        public ParticlesSystemDemo()
        {
            textPosition = new Vector2(400, 200);
            Texture2D smokeTexture = Program.game.Content.Load<Texture2D>("sprites/animations/smoke1_f12w32h32c6r2");
            smokeParticleSystem = new DemoParticles.SmokeParticleSystem(Program.game.SpriteBatch, smokeTexture, 15);
        }

        public override void Update(GameTime gameTime)
        {
            smokeParticleSystem.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

            kState = Keyboard.GetState();

            if (kState.IsKeyDown(Keys.Space) && !oldKState.IsKeyDown(Keys.Space))
            {
                smokeParticleSystem.EmitParticles(new Vector2(220, 150));
            }

            oldKState = kState;
        }

        public override void Draw()
        {
            smokeParticleSystem.Draw();
            Program.game.SpriteBatch.DrawString(Program.game.consolasFont, "Press space to emit particles", textPosition, Color.Black);
        }
    }
}
