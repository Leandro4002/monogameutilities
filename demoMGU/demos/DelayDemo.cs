﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace DemoMGU
{
    class DelayDemo : Demo
    {
        Delay sampleDelay;
        Vector2 textPosition;

        public DelayDemo()
        {
            textPosition = new Vector2(200);
            sampleDelay = new Delay(1); //value in seconds
        }

        public override void Update(GameTime gameTime)
        {
            sampleDelay.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

            if (Keyboard.GetState().IsKeyDown(Keys.Space) && sampleDelay.isTrigger)
            {
                sampleDelay.Reset();
            }
        }

        public override void Draw()
        {
            string text = "Press the space bar to reset the delay\n\n" +
                "sampleDelay.isTrigger: " + sampleDelay.isTrigger + "\n" +
                "sampleDelay.handler: " + Math.Floor(sampleDelay.handler * 1000) + " ms\n" +
                "sampleDelay.timer: " + Math.Floor(sampleDelay.timer * 1000) + " ms\n";
            Program.game.SpriteBatch.DrawString(Program.game.consolasFont, text, textPosition, Color.Black);
        }
    }
}
