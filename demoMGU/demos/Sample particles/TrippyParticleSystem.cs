﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameUtilities;

namespace DemoParticles {
    public class TrippyParticleSystem : ParticleSystem {
        float time, timeSpeed = 3f, amplitude = 100;
        public TrippyParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps) : base(spriteBatch, texture, particlesFps) {
            
        }

        protected override void InitializeConstants() {
            minScale = 0.8f; maxScale = 0.8f;
            minLifeDuration = 2.5f; maxLifeDuration = 2.5f;
            minAcceleration = 0; maxAcceleration = 0;
            minInitialSpeed = 300; maxInitialSpeed = 300;
            minRotationSpeed = 0; maxRotationSpeed = 0;
            minInitialRotation = 0; maxInitialRotation = 0;
            minNumParticles = 1; maxNumParticles = 1;
            TotalNumberOfParticles = 150;
            emitAngle = 0;
            emitDirection = 2.356f;
            ParticlesColor = new Color(255, 255, 255, 255);
            fadeOutMethod = ParticleSystem.FadeOutMethod.None;
            deathTrigger = ParticleSystem.DeathTrigger.onEndOfLifeDuration;
        }

        public override void Update(float dt) {
            base.Update(dt);
            time += timeSpeed * dt;
        }

        protected override void InitializeParticle(Particle p, Vector2 pos) {
            base.InitializeParticle(p, pos);
            p.isLooping = true;
            p.position.Y += amplitude * (float)Math.Sin(time);
        }
    }
}