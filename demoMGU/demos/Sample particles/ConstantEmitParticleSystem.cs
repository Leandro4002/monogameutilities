﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameUtilities;

namespace DemoParticles {
    public class ConstantEmitParticleSystem : ParticleSystem {
        public ConstantEmitParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps) : base(spriteBatch, texture, particlesFps) {
            
        }

        protected override void InitializeConstants() {
            minScale = 0.5f; maxScale = 0.8f;
            minLifeDuration = 2; maxLifeDuration = 2;
            minAcceleration = -20; maxAcceleration = -50;
            minInitialSpeed = 130; maxInitialSpeed = 160;
            minRotationSpeed = 0; maxRotationSpeed = 0;
            minInitialRotation = 0; maxInitialRotation = 6.2832f;
            minNumParticles = 10; maxNumParticles = 20;
            TotalNumberOfParticles = 500;
            emitAngle = 6.2832f;
            emitDirection = 0.78f;
            fadeOutMethod = ParticleSystem.FadeOutMethod.FunctionalBezier;
            fadePoints = new Vector2[] { new Vector2(0, 0.02f), new Vector2(0.25f, 2.15f), new Vector2(0.75f, 0), new Vector2(1, 0) };
            deathTrigger = ParticleSystem.DeathTrigger.OnEndOfAnimation;
        }
    }
}