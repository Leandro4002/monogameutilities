﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameUtilities;

namespace DemoParticles {
    public class SmokeParticleSystem : ParticleSystem {
        public SmokeParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps) : base(spriteBatch, texture, particlesFps) {
            
        }

        protected override void InitializeConstants() {
            minScale = 2.5f; maxScale = 3;
            minLifeDuration = 0.8f; maxLifeDuration = 1;
            minAcceleration = -250; maxAcceleration = -300;
            minInitialSpeed = 300; maxInitialSpeed = 400;
            minRotationSpeed = 0; maxRotationSpeed = 0;
            minInitialRotation = 0; maxInitialRotation = 0;
            minNumParticles = 15; maxNumParticles = 20;
            TotalNumberOfParticles = 50;
            emitAngle = 1.43f;
            emitDirection = 0.785f;
            fadeOutMethod = ParticleSystem.FadeOutMethod.Linear;
            fadePoints = new Vector2[] { new Vector2(0, 0.64f), new Vector2(0.75f, 0.52f), new Vector2(1, 0) };
            deathTrigger = ParticleSystem.DeathTrigger.onEndOfLifeDuration;
        }
    }
}