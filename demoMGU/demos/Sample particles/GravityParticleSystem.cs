﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace DemoParticles {
    public class GravityParticleSystem : ParticleSystem {
        public GravityParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps) : base(spriteBatch, texture, particlesFps) {
            
        }

        protected override void InitializeConstants() {
            minScale = 1; maxScale = 1;
            minLifeDuration = 2; maxLifeDuration = 2;
            minAcceleration = 0; maxAcceleration = 0;
            minInitialSpeed = 200; maxInitialSpeed = 200;
            minRotationSpeed = -1; maxRotationSpeed = 0;
            minInitialRotation = 0; maxInitialRotation = 6.2832f;
            minNumParticles = 1; maxNumParticles = 3;
            TotalNumberOfParticles = 15;
            emitAngle = 6.2832f;
            emitDirection = 0;
            fadeOutMethod = ParticleSystem.FadeOutMethod.Linear;
            fadePoints = new Vector2[] { new Vector2(0, 1f), new Vector2(0.84f, 1), new Vector2(1, 0f) };
            deathTrigger = ParticleSystem.DeathTrigger.onEndOfLifeDuration;
            CustomParticleUpdate = (Particle p, float dt) => {
                float gravityForce = 10;
                Vector2 delta = origin - p.position;
                p.velocity += delta * gravityForce * dt;
                return p;
            };
        }

        public override void Update(float dt) {
            base.Update(dt);
            origin = Mouse.GetState().Position.ToVector2();
        }
    }
}