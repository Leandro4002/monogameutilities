﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameUtilities;

namespace DemoParticles {
    public class RainParticleSystem : ParticleSystem {
        public RainParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps) : base(spriteBatch, texture, particlesFps) {
            
        }

        protected override void InitializeConstants() {
            minScale = 1; maxScale = 2;
            minLifeDuration = 2; maxLifeDuration = 2;
            minAcceleration = 0; maxAcceleration = 0;
            minInitialSpeed = 400; maxInitialSpeed = 800;
            minRotationSpeed = 0; maxRotationSpeed = 0;
            minInitialRotation = 0; maxInitialRotation = 0;
            minNumParticles = 3; maxNumParticles = 3;
            TotalNumberOfParticles = 500;
            emitAngle = 0;
            emitDirection = 0.785f;
            fadeOutMethod = ParticleSystem.FadeOutMethod.None;
            deathTrigger = ParticleSystem.DeathTrigger.onEndOfLifeDuration;
        }

        protected override void InitializeParticle(Particle p, Vector2 pos) {
            base.InitializeParticle(p, pos);
            p.position = new Vector2(random.Next(0, spriteBatch.GraphicsDevice.Viewport.Width), -p.frameHgt * p.scale.Y);

            // Far drops seems smaller and slower and closer drops seems bigger and faster
            float depth = Tools.Map(p.scale.Y, minScale, maxScale, 0, 1);
            p.velocity.Y = Tools.Map(depth, 0, 1, minInitialSpeed, maxInitialSpeed);
            p.color = Color.Lerp(Color.Purple, Color.DeepPink, depth);
            p.layerDepth = depth;

            // Life duration needed to move across the window
            p.lifeDuration = (spriteBatch.GraphicsDevice.Viewport.Height + p.frameHgt * 1.5f * p.scale.Y) / p.velocity.Y;
        }
    }
}