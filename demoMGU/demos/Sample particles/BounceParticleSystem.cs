﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameUtilities;

namespace DemoParticles {
    public class BounceParticleSystem : ParticleSystem {
        public BounceParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps) : base(spriteBatch, texture, particlesFps) {
            
        }

        protected override void InitializeConstants() {
            minScale = 2; maxScale = 2;
            minLifeDuration = 2; maxLifeDuration = 2;
            minAcceleration = -150; maxAcceleration = -150;
            minInitialSpeed = 150; maxInitialSpeed = 150;
            minRotationSpeed = 0; maxRotationSpeed = 0;
            minInitialRotation = 0; maxInitialRotation = 0;
            minNumParticles = 70; maxNumParticles = 70;
            TotalNumberOfParticles = 70;
            emitAngle = 6.2832f;
            emitDirection = 0.78f;
            fadeOutMethod = ParticleSystem.FadeOutMethod.Linear;
            fadePoints = new Vector2[] { new Vector2(0, 1), new Vector2(1, 0) };
            deathTrigger = ParticleSystem.DeathTrigger.OnEndOfAnimation;
        }
    }
}