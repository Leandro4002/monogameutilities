﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace DemoParticles {
    public class HomingParticleSystem : ParticleSystem {
        public HomingParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps) : base(spriteBatch, texture, particlesFps) {
            
        }

        protected override void InitializeConstants() {
            minScale = 1; maxScale = 1.5f;
            minLifeDuration = 0.8f; maxLifeDuration = 1.2f;
            minAcceleration = 0; maxAcceleration = 0;
            minInitialSpeed = 300; maxInitialSpeed = 350;
            minRotationSpeed = 0; maxRotationSpeed = 0;
            minInitialRotation = 0; maxInitialRotation = 6.2832f;
            minNumParticles = 1; maxNumParticles = 1;
            TotalNumberOfParticles = 70;
            CustomParticleDirection = (random) => {
                MouseState mouseState = Mouse.GetState();
                Vector2 delta = mouseState.Position.ToVector2() - this.origin;
                delta.Normalize();
                return delta;
            };
            fadeOutMethod = ParticleSystem.FadeOutMethod.Linear;
            fadePoints = new Vector2[] { new Vector2(0, 1), new Vector2(0.75f, 0.97f), new Vector2(1, 0.02f) };
            deathTrigger = ParticleSystem.DeathTrigger.onEndOfLifeDuration;
        }
    }
}