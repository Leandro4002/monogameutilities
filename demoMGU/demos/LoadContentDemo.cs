﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace DemoMGU
{
    class LoadContentDemo : Demo
    {
        Dictionary<string, SpriteFont> fonts;
        Dictionary<string, Texture2D> animations;
        Dictionary<string, Texture2D> images;
        string text;
        public LoadContentDemo()
        {
            fonts = Program.game.Content.LoadPath<SpriteFont>("Content/fonts/");
            animations = Program.game.Content.LoadPath<Texture2D>("Content/sprites/animations");
            images = Program.game.Content.LoadPath<Texture2D>("Content/sprites/images/");

            text = "Dictionary<string, SpriteFont> fonts:\n";
            foreach (KeyValuePair<string, SpriteFont> val in fonts) text += " " + val.Key + "\n";

            text += "\nDictionary<string, Texture2D> animations:\n";
            foreach (KeyValuePair<string, Texture2D> val in animations) text += " " + val.Key + "\n";

            text += "\nDictionary<string, Texture2D> images:\n";
            foreach (KeyValuePair<string, Texture2D> val in images) text += " " + val.Key + "\n";
        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw()
        {
            Program.game.SpriteBatch.DrawString(Program.game.consolasFont, text, new Vector2(100), Color.Black);

            Program.game.SpriteBatch.DrawString(Program.game.consolasFont, "images[\"wallhaven - 8198\"]", new Vector2(400, 185), Color.Black);
            Program.game.SpriteBatch.Draw(images["wallhaven-8198"], new Vector2(400, 200), null, Color.White, 0, Vector2.Zero, new Vector2(0.2f), SpriteEffects.None, 0);
        }
    }
}
