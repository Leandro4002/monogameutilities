﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace DemoMGU
{
    public class DemoMGU : Game
    {
        public GraphicsDeviceManager Graphics;
        public SpriteBatch SpriteBatch;
        public KeyboardState kState, oldKState;
        Demo currentDemo;
        int currentDemoIndex;
        string helpText;
        Type[] demos;
        public SpriteFont consolasFont;

        public DemoMGU()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            demos = (from t in Assembly.GetExecutingAssembly().GetTypes()
                     where t.IsSubclassOf(typeof(Demo))
                     select t as Type).ToArray();

            currentDemoIndex = 0;
            LoadDemo(currentDemoIndex);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            consolasFont = Content.Load<SpriteFont>("fonts/consolas");
        }

        protected override void Update(GameTime gameTime)
        {
            kState = Keyboard.GetState();

            if (kState.IsKeyDown(Keys.Escape)) Exit();
            if (kState.IsKeyDown(Keys.Left) && !oldKState.IsKeyDown(Keys.Left)) {
                currentDemoIndex--;
                if (currentDemoIndex < 0) currentDemoIndex = demos.Length - 1;
                LoadDemo(currentDemoIndex);
            }
            if (kState.IsKeyDown(Keys.Right) && !oldKState.IsKeyDown(Keys.Right))
            {
                currentDemoIndex++;
                if (currentDemoIndex > demos.Length - 1) currentDemoIndex = 0;
                LoadDemo(currentDemoIndex);
            }

            currentDemo.Update(gameTime);

            oldKState = kState;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            SpriteBatch.Begin(samplerState: SamplerState.PointClamp);
            currentDemo.Draw();
            SpriteBatch.DrawString(consolasFont, helpText, Vector2.Zero, Color.Black);
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        void LoadDemo(int demoIndex)
        {
            currentDemo = (Demo)Activator.CreateInstance(demos[demoIndex]);
            helpText = "Current demo : " + (currentDemoIndex + 1) + " / " + demos.Length + "\n" + currentDemo.GetType().ToString();
        }
    }
}
