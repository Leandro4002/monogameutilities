﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoMGU
{
    public abstract class Demo
    {
        public abstract void Update(GameTime gameTime);
        public abstract void Draw();
    }
}
