//Adapted from
//https://github.com/CartBlanche/MonoGame-Samples/blob/master/ParticleSample/ParticleSystem.cs

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace MonoGameUtilities {
    public abstract class ParticleSystem {
        public enum FadeOutMethod { None, Linear, FunctionalBezier };
        public enum DeathTrigger { onEndOfLifeDuration, OnEndOfAnimation };
        public SpriteBatch spriteBatch;
        public Vector2 origin = Vector2.Zero;
        public bool showDebug;
        int particlesFps;
        Texture2D texture, debugOriginTexture, debugParticleOriginTexture;
        Particle[] particles;
        Queue<Particle> freeParticles;
        Color particlesColor = Color.White;
        protected Random random;
        public int TotalNumberOfParticles {
            get => particles.Length;
            set {
                ConstructData(value);
            }
        }
        public int ParticlesFps {
            get => particlesFps;
            set {
                particlesFps = value;
                foreach (Particle particle in particles) particle.fps = particlesFps;
            }
        }
        public Color ParticlesColor {
            get => particlesColor;
            set {
                particlesColor = value;
                foreach (Particle particle in particles) particle.color = particlesColor;
            }
        }
        public Texture2D Texture {
            get => texture;
            set {
                texture = value;
                foreach (Particle particle in particles) {
                    particle.texture = texture;
                    particle.origin = new Vector2(particle.frameWth / 2, particle.frameHgt / 2);
                }
            }
        }
        public Particle[] Particles {
            get => particles;
        }

        #region Constants to be set by subclasses
        public int minNumParticles, maxNumParticles; // Number of particles created in "AddParticles" method
        public float minInitialSpeed, maxInitialSpeed,
            minAcceleration, maxAcceleration,
            minRotationSpeed, maxRotationSpeed,
            minLifeDuration, maxLifeDuration,
            minScale, maxScale,
            minInitialRotation, maxInitialRotation,
            emitAngle, emitDirection;
        public Vector2[] fadePoints; // List of points that describe a curve used to determine particles alpha value over time
        public DeathTrigger deathTrigger;
        public FadeOutMethod fadeOutMethod;
        #endregion

        public ParticleSystem(SpriteBatch spriteBatch, Texture2D texture, int particlesFps, Random random = null) {
            this.spriteBatch = spriteBatch;
            this.texture = texture;
            this.particlesFps = particlesFps;
            this.random = random ?? new Random();

            debugOriginTexture = spriteBatch.GraphicsDevice.CreateCircleTexture(4, Color.Green);
            debugParticleOriginTexture = spriteBatch.GraphicsDevice.CreateCircleTexture(10, Color.Red);

            InitializeConstants();
        }

        protected abstract void InitializeConstants();

        public virtual void Update(float dt) {
            for (int i = 0; i < particles.Length; i ++) {
                if (IsParticleAlive(particles[i])) {
                    particles[i].Update(dt);
                    if (CustomParticleUpdate != null) particles[i] = CustomParticleUpdate.Invoke(particles[i], dt);
                    if (!IsParticleAlive(particles[i])) freeParticles.Enqueue(particles[i]);
                }
            }
        }

        public void Draw() {
            foreach (Particle p in particles) {
                if (!IsParticleAlive(p)) continue;
                Color particleColor = p.color;

                if (fadeOutMethod != FadeOutMethod.None) {
                    float lifetime;
                    // lifetime: 0=begining of particle's life   1=end of particle's life
                    switch (deathTrigger) {
                        case DeathTrigger.onEndOfLifeDuration: lifetime = p.timeSinceStart / p.lifeDuration; break;
                        case DeathTrigger.OnEndOfAnimation: lifetime = p.timeSinceStart / (1f / p.fps * p.totalFrames); break;
                        default: throw new NotImplementedException();
                    }

                    switch (fadeOutMethod) {
                        case FadeOutMethod.FunctionalBezier: particleColor.A = (byte)(CalculateOpacityFunctionalBezier(fadePoints, lifetime) * p.color.A); break;
                        case FadeOutMethod.Linear: particleColor.A = (byte)(CalculateOpacityLinear(fadePoints, lifetime) * p.color.A); break;
                        default: throw new NotImplementedException();
                    }
                }
                float alpha = (float)particleColor.A / 255f;
                particleColor.A = 255;
                p.Draw(spriteBatch, p.position, particleColor * alpha, layerDepth: p.layerDepth);
            }

            if (showDebug) {
                foreach (Particle p in particles) {// We iterate through all the particles here so we do not have to check showDebug in the iteration above when in non-debug mode
                    spriteBatch.Draw(debugOriginTexture, p.position - new Vector2(debugOriginTexture.Width / 2), IsParticleAlive(p) ? Color.White : Color.Gray);
                    spriteBatch.DrawLine(p.position, p.position + p.velocity / 8, Color.Yellow);
                }
                spriteBatch.Draw(debugParticleOriginTexture, origin - new Vector2(debugParticleOriginTexture.Width / 2), Color.White);
            }
        }

        public void EmitParticles(Vector2? pos = null) {
            int numParticles = random.Next(minNumParticles > maxNumParticles ? maxNumParticles : minNumParticles, maxNumParticles);

            //Create that many particles, if there is enough free particles
            for (int i = 0; i < numParticles && freeParticles.Count > 0; i++) {
                Particle p = freeParticles.Dequeue();
                InitializeParticle(p, pos ?? Vector2.Zero + origin);
            }
        }

        protected virtual void InitializeParticle(Particle p, Vector2 pos) {
            Vector2 direction;
            if (CustomParticleDirection == null) {
                float minAngle = emitDirection - emitAngle / 2, maxAngle = emitDirection + emitAngle / 2;
                float choosedAngle = minAngle + (float)random.NextDouble() * (maxAngle - minAngle);
                double cos = Math.Cos(choosedAngle), sin = Math.Sin(choosedAngle);
                direction = new Vector2((float)(cos - sin), (float)(cos + sin));
            } else {
                direction = CustomParticleDirection.Invoke(random);
            }

            float velocity = random.RandomFloatBetween(minInitialSpeed, maxInitialSpeed);
            float acceleration = random.RandomFloatBetween(minAcceleration, maxAcceleration);
            float lifeDuration = random.RandomFloatBetween(minLifeDuration, maxLifeDuration);
            float scale = random.RandomFloatBetween(minScale, maxScale);
            float rotationSpeed = random.RandomFloatBetween(minRotationSpeed, maxRotationSpeed);
            float initialRotation = random.RandomFloatBetween(minInitialRotation, maxInitialRotation);

            p.Initialize(
                pos, velocity * direction, acceleration * direction,
                lifeDuration, scale, rotationSpeed, initialRotation);
        }

        public Func<Random, Vector2> CustomParticleDirection = null;
        public Func<Particle, float, Particle> CustomParticleUpdate = null;

        public bool IsParticleAlive(Particle p) {
            switch (deathTrigger) {
                case DeathTrigger.onEndOfLifeDuration: return p.timeSinceStart < p.lifeDuration;
                case DeathTrigger.OnEndOfAnimation: return p.isActive;
                default: throw new NotImplementedException();
            }
        }
        public void ConstructData(int totalNumOfParticles) {
            if (totalNumOfParticles < 0) throw new ArgumentException("totalNumOfParticles must be bigger or equal to 0", "totalNumOfParticles");
            particles = new Particle[totalNumOfParticles];
            freeParticles = new Queue<Particle>(totalNumOfParticles);
            for (int i = 0; i < particles.Length; i++) {
                particles[i] = new Particle(texture, particlesFps);
                particles[i].color = particlesColor;
                freeParticles.Enqueue(particles[i]);
            }
        }

        public static float CalculateOpacityLinear(Vector2[] points, float lifetime) {
            System.Diagnostics.Debug.Assert(lifetime >= 0 && lifetime <= 1);
            System.Diagnostics.Debug.Assert(points[0].X == 0);
            System.Diagnostics.Debug.Assert(points[points.Length - 1].X == 1);
            for (int i = 1; i < points.Length; i++) {
                System.Diagnostics.Debug.Assert(points[i].X >= points[i - 1].X);
            }

            for (int i = 1; i < points.Length; i++) {
                if (points[i].X < lifetime) continue;

                return Tools.Map(lifetime, points[i - 1].X, points[i].X, points[i - 1].Y, points[i].Y);
            }

            throw new Exception("Unexpected error");
        }
        public static float CalculateOpacityFunctionalBezier(Vector2[] points, float lifetime) {
            System.Diagnostics.Debug.Assert(lifetime >= 0 && lifetime <= 1);
            System.Diagnostics.Debug.Assert(points[0].X == 0);
            System.Diagnostics.Debug.Assert(points[points.Length - 1].X == 1);

            float opacity = FunctionalBezier.CalculateAt(lifetime, points[0].Y, points[1].Y, points[2].Y, points[3].Y);
            return MathHelper.Clamp(opacity, 0, 1);
        }
    }
}