﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace MonoGameUtilities {
    public static class Extensions {
        #region Vector3
        // Retrurns a new Vector with only the selected value changed
        public static Vector3 ChangeX(this Vector3 self, float x) => new Vector3(x, self.Y, self.Z);
        public static Vector3 ChangeY(this Vector3 self, float y) => new Vector3(self.X, y, self.Z);
        public static Vector3 ChangeZ(this Vector3 self, float z) => new Vector3(self.X, self.Y, z);
        #endregion

        #region AnimatedSprite
        public static AnimatedSprite centerAnimation(this AnimatedSprite anim) {
            anim.origin.X = anim.frameWth / 2;
            anim.origin.Y = anim.frameHgt / 2;
            return anim;
        }
        public static AnimatedSprite drawParam(this AnimatedSprite anim, Color? color = null, SpriteEffects? spriteEffects = null) {
            anim.color = color ?? anim.color;
            anim.spriteEffects = spriteEffects ?? anim.spriteEffects;
            return anim;
        }
        public static AnimatedSprite animParam(this AnimatedSprite anim, bool? isLooping = null, bool? isActive = null, bool? isVisible = null) {
            anim.isVisible = isVisible ?? anim.isVisible;
            anim.isLooping = isLooping ?? anim.isLooping;
            anim.isActive = isActive ?? anim.isActive;
            return anim;
        }
        public static AnimatedSprite transformParam(this AnimatedSprite anim, float? rotation = null, Vector2? origin = null, Vector2? scale = null) {
            anim.rotation = rotation ?? anim.rotation;
            anim.origin = origin ?? anim.origin;
            anim.scale = scale ?? anim.scale;
            return anim;
        }
        public static void DrawAnim(this SpriteBatch spriteBatch, AnimatedSprite animatedSprite, Vector2? position, Color? otherColor = null, SpriteEffects? spriteEffects = null, float layerDepth = 0) {
            animatedSprite.Draw(spriteBatch, position, otherColor, spriteEffects, layerDepth);
        }
        #endregion

        #region Graphics
        //https://community.monogame.net/t/whats-the-simplest-way-to-draw-a-rectangular-outline-without-generating-the-texture/7818/5
        private static Texture2D _blankTexture;
        public static Texture2D GetBlankTexture(this GraphicsDevice graphicsDevice) {
            if (_blankTexture == null) {
                _blankTexture = new Texture2D(graphicsDevice, 1, 1);
                _blankTexture.SetData(new[] { Color.White });
            }
            return _blankTexture;
        }
        public static Texture2D GetBlankTexture(this SpriteBatch spriteBatch) {
            return GetBlankTexture(spriteBatch.GraphicsDevice);
        }

        // Taken from https://github.com/craftworkgames/MonoGame.Extended `spriteBatch.FillRectangle`
        public static void DrawRectangle(this SpriteBatch spriteBatch, Rectangle rect, Color color, float rotation = 0, Vector2? origin = null, SpriteEffects spriteEffects = SpriteEffects.None, float layerDepth = 0) {
            spriteBatch.Draw(spriteBatch.GraphicsDevice.GetBlankTexture(), rect, null, color, rotation, origin ?? Vector2.Zero, spriteEffects, layerDepth);
        }

        //https://community.monogame.net/t/line-drawing/6962/5
        public static void DrawLine(this SpriteBatch spriteBatch, Vector2 point1, Vector2 point2, Color color, float thickness = 1f) {
            float distance = Vector2.Distance(point1, point2);
            float angle = (float)System.Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            DrawLine(spriteBatch, point1, distance, angle, color, thickness);
        }
        private static Vector2 lineOrigin = new Vector2(0f, 0.5f);
        public static void DrawLine(this SpriteBatch spriteBatch, Vector2 point, float length, float angle, Color color, float thickness = 1f) {
            Vector2 scale = new Vector2(length, thickness);
            spriteBatch.Draw(spriteBatch.GetBlankTexture(), point, null, color, angle, lineOrigin, scale, SpriteEffects.None, 0);
        }
        #endregion
    }
}
