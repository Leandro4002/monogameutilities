# MonoGame utilities
Set of simple and helpful utilities for MonoGame C#  
*I often use a variable named `dt`, it means "delta time" and it is the number of seconds elapsed since last frame.*

## Animated sprite
<img src="https://i.imgur.com/Qh1vosc.gif" width="300" height="180" />

```csharp
AnimatedSprite anim;

void Initialize() {
    Texture2D spritesheetTexture = Content.Load<Texture2D>("path/to/my/spritesheet");
    anim = new AnimatedSprite(spritesheetTexture, 15).animParam(isLooping: true);
    anim.scale = new Vector2(2);
}

void Update(GameTime gameTime) {
    anim.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
}

void Draw() {
    _spritebatch.Begin();
    anim.Draw(_spriteBatch, explositionAnimationPos);
    _spritebatch.End();
}
```

## Shapes collisions and textures creation
<img src="https://i.imgur.com/FT7ekUB.gif" width="300" height="180" /><br/>

```csharp
Texture2D circleTexture, rectTexture;
Vector2 circlePos, rectPos;
int rectSize, circleRadius;

void Initialize() {
    rectTexture = Tools.CreateRectTexture(GraphicsDevice, rectSize, rectSize, Color.Red);
    circleTexture = Tools.CreateCircleTexture(GraphicsDevice, circleRadius, Color.Blue);
    circlePos = new Vector2(150, 100);
    rectPos = new Vector2(200, 100);
}

void Update(GameTime gameTime) {
    bool isColliding = Tools.Circle2Rect(circlePos.X, circlePos.Y, circleRadius, rectPos.X, rectPos.Y, rectSize, rectSize);
}

void Draw() {
    _spriteBatch.Draw(circleTexture, circlePos, Color.White);
    _spriteBatch.Draw(rectTexture, rectPos, Color.White);
}
```

## Delay
```csharp
Delay sampleDelay;

void Initialize() {
    sampleDelay = new Delay(1); //value in seconds
}

void Update(GameTime gameTime) {
    sampleDelay.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

    if (Keyboard.GetState().IsKeyDown(Keys.Space) && sampleDelay.isTrigger) {
        sampleDelay.Reset();
    }
}
```

## Particle system
<img src="https://i.imgur.com/OzNqAUJ.gif" width="300" height="180" /><br/>

<img src="https://i.imgur.com/0TWlpXL.gif" width="137" height="175" />
<img src="https://i.imgur.com/5Uifm75.gif" width="137" height="175" />
<img src="https://i.imgur.com/cGPRf6Q.gif" width="137" height="175" />
<img src="https://i.imgur.com/oxzFXY6.gif" width="137" height="175" />
<img src="https://i.imgur.com/IX0W0uy.gif" width="137" height="175" />
<img src="https://i.imgur.com/eaFigfp.gif" width="137" height="175" />
<img src="https://i.imgur.com/Xd95ESy.gif" width="137" height="175" />

*See [ParticlesEditor](https://gitlab.com/Leandro4002/particleseditor) to create your own particle system*

```csharp
SampleParticleSystem sampleParticleSystem;

void Initialize() {
    Texture2D sampleParticleTexture = Content.Load<Texture2D>("path/to/my/spritesheet");
    sampleParticleSystem = new SampleParticleSystem(GraphicsDevice, 5, sampleParticleTexture, new Random(), 10);
}

void Update(GameTime gameTime) {
    sampleParticleSystem.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

    if (Keyboard.GetState().IsKeyDown(Keys.Space)) {
        sampleParticleSystem.AddParticles(new Vector2(300, 300));
    }
}

void Draw() {
    sampleParticleSystem.Draw();
}
```

## Load every content in a path
```csharp
Dictionary<string, SpriteFont> fonts;
Dictionary<string, Texture2D> animations;
Dictionary<string, Texture2D> images;
void Initialize () {
    fonts = Tools.LoadContentInPath<SpriteFont>(Program.game.Content, "Content/fonts/");
    animations = Tools.LoadContentInPath<Texture2D>(Program.game.Content, "Content/sprites/animations/");
    images = Tools.LoadContentInPath<Texture2D>(Program.game.Content, "Content/sprites/images/");
}
```

## Bezier curves
<img src="https://i.imgur.com/PMD8MBj.gif" width="300" height="180" />

```csharp
Vector2[] points;
Texture2D quadraticCurve, cubicCurve;
Vector2 quadraticCurvePos, cubicCurvePos;
int textureSize = 200, curvePosition = 300;

void Initialize() {
    quadraticCurvePos = new Vector2(100, 100);
    points = new Vector2[] { new Vector2(0, 0), new Vector2(.7f, .2f), new Vector2(1, 1) };
    quadraticCurve = QuadraticBezier.GenerateCurve(GraphicsDevice, textureSize, curvePosition, Color.Black, points, false);

    cubicCurvePos = new Vector2(400, 100);
    points = new Vector2[] { new Vector2(0, 0), new Vector2(.7f, .2f), new Vector2(.1f, .6f), new Vector2(1, 1) };
    cubicCurve = CubicBezier.GenerateCurve(GraphicsDevice, textureSize, curvePosition, Color.Red, points, true);
}

void Draw() {
    _spriteBatch.Draw(cubicCurve, cubicCurvePos, Color.White);
    _spriteBatch.Draw(quadraticCurve, quadraticCurvePos, Color.White);
}
```