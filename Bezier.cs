﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameUtilities {
    //We don't use Math.Pow because it could decrease performance
    //The GenerateCurve method is duplicated because I didn't find a clean way to do otherwise

    public static class QuadraticBezier {
        //https://javascript.info/bezier-curve
        //(1-t)^2*p1 + 2*(1-t)*t*p2 + t^2*p3
        public static Vector2 CalculateAt(float t, Vector2 p1, Vector2 p2, Vector2 p3) => ((1-t)*(1-t))*p1 + 2*(1-t)*t*p2 + (t*t)*p3;
        public static float CalculateAt(float t, float p1, float p2, float p3) => ((1-t)*(1-t))*p1 + 2*(1-t)*t*p2 + (t*t)*p3;

        //Replicated method of CubicBezier
        public static Texture2D GenerateCurve(GraphicsDevice gd, int textureSize, int precision, Color color, Vector2[] points, bool bigLine = false, bool invertYAxis = false) {
            if (points == null || points.Length != 3) throw new ArgumentException("You must provide a Vector2 array of length 3", "points");
            Texture2D texture = new Texture2D(gd, textureSize, textureSize);
            int numberOfPixels = textureSize * textureSize;
            Color[] data = new Color[numberOfPixels];
            if (invertYAxis) for (int i = 0; i < points.Length; i++) points[i].Y = 1 - points[i].Y;
            for (int i = 0; i < precision; i++) {
                float t = (float)i / (float)precision;
                Vector2 currentPos = CalculateAt(t, points[0], points[1], points[2]) * textureSize;
                int calculatedPos = (int)((int)(currentPos.Y) * textureSize) + (int)currentPos.X;

                if (calculatedPos >= data.Length) calculatedPos = data.Length - 1;
                data[calculatedPos] = color;

                if (bigLine) {
                    int up = calculatedPos - textureSize;
                    int left = calculatedPos - 1;
                    int down = calculatedPos + textureSize;
                    int right = calculatedPos + 1;

                    if (up >= 0) data[up] = color;
                    if (left >= 0 && left/textureSize == calculatedPos/textureSize) data[left] = color;
                    if (down < data.Length) data[down] = color;
                    if (right < data.Length && right/textureSize == calculatedPos/textureSize) data[right] = color;
                }
            }
            if (invertYAxis) for (int i = 0; i < points.Length; i++) points[i].Y = 1 - points[i].Y;
            texture.SetData(data);
            return texture;
        }
    }

    public static class CubicBezier {
        //https://javascript.info/bezier-curve
        //(1-t)^3*p1 + 3*(1-t)^2*t*p2 + 3*(1-t)*t^2*p3 + t^3*p4

        //http://mathfaculty.fullerton.edu/mathews/n2003/BezierCurveMod.html
        //p1 + 3*t*(p2-p1) + 3*t^2*(p1 + p3 - 2*p2) + t^3*(p4 - p1 + 3*p2 - 3*p3)

        public static Vector2 CalculateAt(float t, Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) => ((1-t)*(1-t)*(1-t))*p1 + 3*((1-t)*(1-t))*t*p2 + 3*(1-t)*(t*t)*p3 + (t*t*t)*p4;
        public static float CalculateAt(float t, float p1, float p2, float p3, float p4) => ((1-t)*(1-t)*(1-t))*p1 + 3*((1-t)*(1-t))*t*p2 + 3*(1-t)*(t*t)*p3 + (t*t*t)*p4;
        //Replicated method of QuadraticBezier
        public static Texture2D GenerateCurve(GraphicsDevice gd, int size, int precision, Color color, Vector2[] points, bool bigLine = false, bool invertYAxis = false) {
            if (points == null || points.Length != 4) throw new ArgumentException("You must provide a Vector2 array of length 4", "points");
            Texture2D texture = new Texture2D(gd, size, size);
            int numberOfPixels = size * size;
            Color[] data = new Color[numberOfPixels];
            if (invertYAxis) for (int i = 0; i < points.Length; i++) points[i].Y = 1 - points[i].Y;
            for (int i = 0; i < precision; i++) {
                float t = (float)i / (float)precision;
                Vector2 currentPos = CalculateAt(t, points[0], points[1], points[2], points[3]) * size;
                int calculatedPos = (int)((int)(currentPos.Y) * size) + (int)currentPos.X;

                if (calculatedPos >= data.Length) calculatedPos = data.Length - 1;
                data[calculatedPos] = color;

                if (bigLine) {
                    int up = calculatedPos - size;
                    int left = calculatedPos - 1;
                    int down = calculatedPos + size;
                    int right = calculatedPos + 1;

                    if (up >= 0) data[up] = color;
                    if (left >= 0 && left/size == calculatedPos/size) data[left] = color;
                    if (down < data.Length) data[down] = color;
                    if (right < data.Length && right/size == calculatedPos/size) data[right] = color;
                }
            }
            if (invertYAxis) for (int i = 0; i < points.Length; i++) points[i].Y = 1 - points[i].Y;

            texture.SetData(data);
            return texture;
        }
    }

    //https://math.stackexchange.com/a/2571749/810796
    public static class FunctionalBezier {
        public static float CalculateAt(float x, float startY, float easeIn, float easeOut, float endY) {
            // y(x) = (1-x)^3*y0 + 3*x*(1-x)^2*y1 + 3*x^2(1-x)*y2 + x^3*y3
            return (float)(Math.Pow(1 - x, 3) * startY + 3 * x * Math.Pow(1 - x, 2) * easeIn + 3 * Math.Pow(x, 2) * (1 - x) * easeOut + Math.Pow(x, 3) * endY);
}

        public static Texture2D GenerateCurve(GraphicsDevice gd, int size, int precision, Color color, Vector2[] points, bool bigLine = false, bool invertYAxis = false) {
            if (points == null || points.Length != 4) throw new ArgumentException("You must provide a Vector2 array of length 4", "points");
            Texture2D texture = new Texture2D(gd, size, size);
            int numberOfPixels = size * size;
            Color[] data = new Color[numberOfPixels];
            if (invertYAxis) for (int i = 0; i < points.Length; i++) points[i].Y = 1 - points[i].Y;
            for (int i = 0; i < precision; i++) {
                float t = (float)i / (float)precision;
                float yVal = CalculateAt(t, points[0].Y, points[1].Y, points[2].Y, points[3].Y);
                Vector2 currentPos = new Vector2(t, yVal) * size;
                int calculatedPos = (int)((int)(currentPos.Y) * size) + (int)currentPos.X;

                if (calculatedPos < 0 || calculatedPos >= data.Length) continue;

                data[calculatedPos] = color;

                if (bigLine) {
                    int up = calculatedPos - size;
                    int left = calculatedPos - 1;
                    int down = calculatedPos + size;
                    int right = calculatedPos + 1;

                    if (up >= 0) data[up] = color;
                    if (left >= 0 && left / size == calculatedPos / size) data[left] = color;
                    if (down < data.Length) data[down] = color;
                    if (right < data.Length && right / size == calculatedPos / size) data[right] = color;
                }
            }
            if (invertYAxis) for (int i = 0; i < points.Length; i++) points[i].Y = 1 - points[i].Y;

            texture.SetData(data);
            return texture;
        }
    }
}