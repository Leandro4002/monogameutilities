﻿//Adapted from
//https://github.com/CartBlanche/MonoGame-Samples/blob/master/ParticleSample/Particle.cs

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameUtilities {
    public class Particle : AnimatedSprite {
        public Vector2 position, velocity, acceleration;
        public float lifeDuration, timeSinceStart, rotationSpeed, layerDepth;

        public Particle(Texture2D texture, int fps = 10) : base(texture, fps) {
            origin.X = frameWth / 2;
            origin.Y = frameHgt / 2;
        }

        public void Initialize(Vector2 position, Vector2 velocity, Vector2 acceleration,
            float lifeDuration, float scale, float rotationSpeed, float initialRotation, float layerDepth = 0) {
            this.position = position;
            this.velocity = velocity;
            this.acceleration = acceleration;
            this.lifeDuration = lifeDuration;
            this.scale = new Vector2(scale, scale);
            this.rotationSpeed = rotationSpeed;
            this.rotation = initialRotation;
            this.layerDepth = layerDepth;

            currentFrame = 0;
            CalculateCurrentFramePosition();
            timer = 1f / fps;

            this.timeSinceStart = 0.0f;
            this.isActive = true;
        }

        public new void Update(float dt) {
            base.Update(dt);
            velocity += acceleration * dt;
            position += velocity * dt;
            rotation += rotationSpeed * dt;

            timeSinceStart += dt;
        }
    }
}