﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;

namespace MonoGameUtilities {
    public static class Tools {
        #region Collisions
        public static bool Pos2Circle(float x, float y, float circleX, float circleY, float circleRad) {
            //https://stackoverflow.com/a/481150
            return ((x - circleX)*(x - circleX)) + ((y - circleY)*(y - circleY)) < (circleRad*circleRad);
        }
        public static bool Pos2Rect(float x, float y, float rectX, float rectY, float rectWth, float rectHgt) {
            return (x >= rectX && x <= rectX + rectWth) && (y >= rectY && y <= rectY + rectHgt);
        }

        public static bool Rect2Rect(float x, float y, float wth, float hgt, float x2, float y2, float wth2, float hgt2) {
            return (x + wth > x2 && x < x2 + wth2) && (y + hgt > y2 && y < y2 + hgt2);
        }

        public static bool Circle2Circle(float x, float y, float rad, float x2, float y2, float rad2) {
            return Math.Pow(x - x2, 2) + Math.Pow(y - y2, 2) <= Math.Pow(rad + rad2, 2);
        }

        //http://jeffreythompson.org/collision-detection/circle-rect.php
        public static bool Circle2Rect(float cx, float cy, float radius, float rx, float ry, float rw, float rh) {
            // temporary variables to set edges for testing
            float edgeX = cx;
            float edgeY = cy;

            // which edge is closest?
            if (cx < rx)         edgeX = rx;      // test left edge
            else if (cx > rx+rw) edgeX = rx+rw;   // right edge
            if (cy < ry)         edgeY = ry;      // top edge
            else if (cy > ry+rh) edgeY = ry+rh;   // bottom edge

            // get distance from closest edges
            float distX = cx-edgeX;
            float distY = cy-edgeY;
            float distance = (distX*distX) + (distY*distY);

            // if the distance is less than the radius, collision!
            if (distance <= Math.Pow(radius, 2)) {
                return true;
            }
            return false;
        }
        #endregion

        #region Bitmap <---> Texture2D
        public static System.Drawing.Bitmap GetBitmapFromTexture2D(Texture2D texture) {
            var imageStream = new MemoryStream();
            texture.SaveAsPng(imageStream, texture.Width, texture.Height);
            return (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(imageStream);
        }

        //https://stackoverflow.com/a/2870399/11191373
        public static Texture2D GetTexture2DFromBitmap(GraphicsDevice device, System.Drawing.Bitmap bitmap) {
            Texture2D tex = new Texture2D(device, bitmap.Width, bitmap.Height, false, SurfaceFormat.Color);

            System.Drawing.Imaging.BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

            int bufferSize = data.Height * data.Stride;

            byte[] bytes = new byte[bufferSize];

            System.Runtime.InteropServices.Marshal.Copy(data.Scan0, bytes, 0, bytes.Length);

            tex.SetData(bytes);

            bitmap.UnlockBits(data);

            return tex;
        }
        #endregion

        #region Texture2D manipulation
        //If there is the "invalid argument" exception :
        //https://stackoverflow.com/questions/32002182/bitmap-creation-problems-invalid-parameter-c-sharp

        //Argument "points" must be composed of Vector2 with values ranging from 0 to 1
        //By default, X axis goes from left to right and Y axis goes from top to bottom
        public static Texture2D CreateLineTexture(this GraphicsDevice gd, List<Vector2> points, Color color, float scalar = 1, bool crop = false, bool invertYAxis = false) {
            if (points.Count < 2) throw new ArgumentException("A minimum of 2 points is needed to create a line texture", "points");
            if (scalar <= 0) throw new ArgumentException("Cannot have scalar smaller or equal than 0", "scalar");
            foreach(Vector2 point in points) {
                System.Diagnostics.Debug.Assert(point.X <= 1 && point.X >= 0 && point.Y <= 1 && point.Y >= 0);
            }
            float smallestX, smallestY, biggestX, biggestY;
            int width, height;
            if (invertYAxis) {
                for (int i = 0; i < points.Count; i++) {
                    points[i] = new Vector2(points[i].X, 1 - points[i].Y);
                }
            }
            Vector2[] arrayPoints = points.ToArray();

            //Find the smallest/biggest X and Y
            points.Sort((Vector2 a, Vector2 b) => {
                if (a.X < b.X) return -1;
                else if (a.X > b.X) return 1;
                else return 0;
            });
            smallestX = points[0].X;
            biggestX = points[points.Count - 1].X;
            points.Sort((Vector2 a, Vector2 b) => {
                if (a.Y < b.Y) return -1;
                else if (a.Y > b.Y) return 1;
                else return 0;
            });
            smallestY = points[0].Y;
            biggestY = points[points.Count - 1].Y;

            //Added a pixel to the width and the height so the lines drawn at the bottom and at the right are correctly drawn
            width = (int)(biggestX * scalar) + 1;
            height = (int)(biggestY * scalar) + 1;

            //Moves the points and resize texture so the image takes the less space possible
            if (crop) {
                width -= (int)(smallestX * scalar);
                height -= (int)(smallestY * scalar);

                for (int i = 0; i < arrayPoints.Length; i++) {
                    arrayPoints[i].X -= smallestX;
                    arrayPoints[i].Y -= smallestY;
                }
            }

            Color[] data = new Color[width*height];
            Texture2D texture = new Texture2D(gd, width, height);
            
            //Foreach points, get the distance to the next one and draw a line
            for (int i = 0; i < arrayPoints.Length - 1; i++) {
                int pixelsOfDistance = (int)(Vector2.Distance(arrayPoints[i]*scalar, arrayPoints[i + 1]*scalar));
                for (int j = 0; j < pixelsOfDistance; j++) {
                    float currentScalar = (float)j / (float)pixelsOfDistance;
                    Vector2 delta = arrayPoints[i + 1] - arrayPoints[i];
                    Vector2 calculated = arrayPoints[i]*scalar + (delta * currentScalar * scalar);
                    int calculatedPos = (int)((int)(calculated.Y) * width) + (int)calculated.X;

                    //Some calculation of position are incorrect
                    if (calculatedPos > 0 && calculatedPos < data.Length) data[calculatedPos] = color;
                }
            }

            texture.SetData(data);
            return texture;
        }

        public static Texture2D MirrorTexture(this GraphicsDevice gd, Texture2D textureToMirror, bool isVertical = true) {
            throw new NotImplementedException();
            Color[] data = new Color[textureToMirror.Width * textureToMirror.Height];
            Color[] newData = new Color[textureToMirror.Width * textureToMirror.Height];
            textureToMirror.GetData(data);

            for (int i = 0; i < data.Length; i++) {
                int wth = textureToMirror.Width - 1;
            }

            Texture2D newTexture = new Texture2D(gd, textureToMirror.Width, textureToMirror.Height);
            newTexture.SetData(newData);

            return newTexture;
        }

        public static Texture2D CreateCircleTexture(this GraphicsDevice gd, int radius, Color color, int borderThick = 0) {
            if (borderThick >= radius) throw new Exception("Cannot have a border greater or equal than the radius");

            int diameter = radius * 2;
            Texture2D texture = new Texture2D(gd, diameter, diameter);
            int numberOfPixels = diameter * diameter;
            Color[] data = new Color[numberOfPixels];

            for (int i = 0; i < numberOfPixels; i++) {
                int y = i / (diameter);
                int x = i;
                while (x >= diameter) {
                    x -= diameter;
                }

                x = Math.Abs(x - radius);
                y = Math.Abs(y - radius);

                if (x != 0 && y != 0) {
                    double hyp = x / Math.Sin(Math.Atan((float)x / y));

                    if (hyp < radius && (hyp > radius - borderThick || borderThick == 0)) {
                        data[i] = color;
                    }
                } else if (x == 0 && y == radius || y == 0 && x == radius) {
                    //This line doesn't do anything :D
                } else {
                    if (x < radius && (x >= radius - borderThick || borderThick == 0) ||
                        y < radius && (y >= radius - borderThick || borderThick == 0)) {
                        data[i] = color;
                    }
                }
            }

            texture.SetData(data);
            return texture;
        }

        public static Texture2D CreateRectTexture(this GraphicsDevice gd, int width, int height, Color color, int borderThick = 0) {
            Texture2D texture = new Texture2D(gd, width, height);
            int numberOfPixels = width * height;
            Color[] data = new Color[numberOfPixels];

            if (borderThick == 0) {
                for (int i = 0; i < numberOfPixels; i++) {
                    data[i] = color;
                }
            } else {
                if (borderThick < 0) throw new Exception("Cannot have negative rectangle border thickness");
                if (borderThick >= Math.Min(width, height) / 2) throw new Exception("Rectangle border thickness can't be bigger or equal than half of the width or the height");

                //Up
                for (int i = 0; i < width * borderThick; ++i) {
                    data[i] = color;
                }
                //Down
                for (int i = (height - borderThick) * width; i < data.Length; ++i) {
                    data[i] = color;
                }
                //Left
                for (int j = 0; j < borderThick; j++) {
                    for (int i = j; i < data.Length; i += width) {
                        data[i] = color;
                    }
                }
                //Right
                for (int j = 1; j < borderThick + 1; j++) {
                    for (int i = width - j; i < data.Length; i += width) {
                        data[i] = color;
                    }
                }
            }

            texture.SetData(data);
            return texture;
        }

        //Dirty but functioning
        //Need to add border of image
        public static Texture2D CreateGridTexture(this GraphicsDevice gd, int cols, int rows, int size, Color color) {

            Texture2D texture = new Texture2D(gd, cols * size, rows * size);

            int numberOfPixels = cols * size * rows * size;

            //Create an array of the size of the image (1 entry = 1 pixel)
            Color[] data = new Color[numberOfPixels];

            //Fill horizontal lines
            for (int i = size * cols * size; i < numberOfPixels; i += size * cols * size) {
                //Draw a line to the right
                for (int j = 0; j < cols * size; j++) {
                    data[i + j] = color;
                }

                if (i != 0) {
                    i -= size * cols;
                    //Draw a line to the right
                    for (int j = 0; j < cols * size; j++) {
                        data[i + j] = color;
                    }
                    i += size * cols;
                }
            }

            //Fill vertical lines
            for (int i = size; i < cols * size; i += size) {

                //Draw a line to the bottom
                for (int j = 0; j < numberOfPixels; j += cols * size) {
                    data[i + j] = color;
                }

                if (i != 0) {
                    i--;
                    //Draw a line to the bottom
                    for (int j = 0; j < numberOfPixels; j += cols * size) {
                        data[i + j] = color;
                    }
                    i++;
                }
            }

            //Put the data in the texture
            texture.SetData(data);

            return texture;
        }
        #endregion

        #region Other
        public static float RandomFloatBetween(this Random rnd, float min, float max) {
            return (float)rnd.NextDouble() * (max - min) + min;
        }

        public static float BringToZero(float val, float bringForce) {
            if (val > 0 && val - bringForce > 0) {
                return val - bringForce;
            } else if (val < 0 && val + bringForce < 0) {
                return val + bringForce;
            } else {
                return 0;
            }
        }

        public static int BringToZero(int val, int bringForce) {
            if (val > 0 && val - bringForce > 0) {
                return val - bringForce;
            } else if (val < 0 && val + bringForce < 0) {
                return val + bringForce;
            } else {
                return 0;
            }
        }

        //Maps a number from one range to another
        //For example :
        //
        //If we do `Map(5, 0, 10, 0, 100)` output: 50
        //We want to know the what is the equivalent of 5 ranging from 0-5 in a range of 0-100
        //
        //If we do `Map(28, 1, 30, 0, 100)` output: 93,10345
        //We want to know the what is the equivalent of 28 ranging from 1-30 in a range of 0-100
        //We could see that number as the percentage of completion of a month when we are the day 28 (93.103%)
        public static float Map(this float value, float from1, float to1, float from2, float to2) => (value - from1) / (to1 - from1) * (to2 - from2) + from2;

        //Maybe temp
        public static float[] GeneratePerlinNoiseArray(int arraySize, float[] seed, int octaves, float bias) {
            float[] output = new float[arraySize];

            for (int x = 0; x < arraySize; x++) {
                float noise = 0.0f;
                float scale = 1.0f;
                float scaleAcc = 0.0f;

                for (int o = 0; o < octaves; o++) {
                    int pitch = arraySize >> o;
                    int sample1 = (x / pitch) * pitch;
                    int sample2 = (sample1 + pitch) % arraySize;

                    int blend = (x - sample1) / pitch;
                    float sample = (1.0f - blend) * seed[sample1] + blend * seed[sample2];
                    noise += sample * scale;
                    scaleAcc += scale;
                    scale /= bias;
                }

                output[x] = noise / scaleAcc;
            }

            return output;
        }

        public static void ShiftArray<T>(this T[] array, int steps) {
            while (steps != 0) {
                if (steps > 0) {
                    for (int j = 0; j < array.Length - 1; j++) {
                        array[j] = array[j + 1];
                    }
                    steps--;
                } else if (steps < 0) {
                    for (int j = array.Length; j < 1; j--) {
                        array[j] = array[j - 1];
                    }
                    steps++;
                }
            }
        }

        public static Tuple<float, float> quadraticEquation(float a, float b, float c) { //https://stackoverflow.com/a/33454565
            return new Tuple<float, float>(
                (float)((-1 * b + Math.Sqrt(Math.Pow(b, 2) - (4 * a * c))) / (2 * a)),
                (float)((-1 * b - Math.Sqrt(Math.Pow(b, 2) - (4 * a * c))) / (2 * a))
            );
        }

        //Get the length of a line composed of points
        public static float GetLineLength(Vector2[] points) {
            //Can't get line length if there is less than 2 points
            if (points.Length < 2) return 0;

            float lineLength = 0;
            for (int i = 0; i < points.Length - 1; i++) {
                lineLength += Vector2.Distance(points[i], points[i + 1]);
            }

            return lineLength;
        }

        //Get the position of a point in a line composed of points
        //positionInLine: 0=beginning of the line  1=end of the line
        public static Vector2 GetPositionInLine(Vector2[] points, float positionInLine) {
            if (positionInLine < 0 || positionInLine > 1) throw new ArgumentException("Value must be between 0 and 1", "positionInLine");
            float objective = positionInLine * Tools.GetLineLength(points);
            float distance = 0;

            for (int i = 0; i < points.Length - 1; i++) {
                float distanceToNextPoint = Vector2.Distance(points[i], points[i + 1]);
                if (objective < distance + distanceToNextPoint) {
                    float hyp = objective - distance;
                    double radiantAngle = 0;
                    radiantAngle = Math.Atan((points[i].Y - points[i + 1].Y) / (points[i].X - points[i + 1].X));

                    if (points[i].X > points[i + 1].X) {
                        radiantAngle += Math.PI;
                    }

                    double opp = Math.Sin(radiantAngle) * hyp;
                    double adj = Math.Cos(radiantAngle) * hyp;

                    return new Vector2(points[i].X + (float)adj, points[i].Y + (float)opp);
                } else {
                    distance += distanceToNextPoint;
                }
            }

            throw new Exception("Unexpected error");
        }

        /*
        Returns a dictionary indexed by asset location.
        For example if we have this folder tree:
        [Content]
            ┃
            ┣━━[particles]
            ┃      ┣━━explosion.xnb
            ┃      ┣━━fire.xnb
            ┃      ┗━━[special]
            ┃             ┣━━enchant.xnb
            ┃             ┗━━magic.xnb
            ┃
            ┗━━[fonts]
                  ┣━━consolas.xnb
                  ┗━━arial.xnb
        
        and that we use
        particlesTextures = Content.LoadPath<Texture2D>("Content/particles")
        fonts = Content.LoadPath<SpriteFont>("Content/fonts")

        then those dictionaries will contain the following keys
        particlesTextures: "explosion", "fire", "special/enchant", "special/magic"
        fonts: "consolas", "arial"
        */
        public static Dictionary<string, T> LoadPath<T>(this ContentManager contentManager, string path)
        {
            if (path[path.Length - 1] != '/') path += '/';
            Dictionary<string, T> content = new Dictionary<string, T>();

            string[] files = Directory.GetFiles(path, "*.xnb", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                string loadPath = file.Replace(@"\", "/").Replace("Content/", "").Replace(".xnb", "");
                string key = loadPath.Replace(path.Replace("Content/", ""), "");
                content[key] = contentManager.Load<T>(loadPath);
            }

            return content;
        }

        // Similar to clamp (limiting a value between a min and max) but when it is smaller than the min, it loops
        // to the max, same for when it is more than the max
        public static float ClampLoop(float val, float min, float max) {
            float delta = max - min;
            while (val > max) val -= delta;
            while (val < min) val += delta;

            return val;
        }

        public static float EnsureAngleIsInRange(float angle, float angleMaxValue = (float)Math.PI * 2) {
            return ClampLoop(angle, 0, angleMaxValue);
        }
        #endregion
    }
}
