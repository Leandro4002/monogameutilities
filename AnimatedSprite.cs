﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Text.RegularExpressions;

namespace MonoGameUtilities {
    public class AnimatedSprite {
        Texture2D _texture;
        Rectangle sourceRectangle = new Rectangle();
        public int frameX, frameY, cols, rows, frameWth, frameHgt, currentFrame, totalFrames, fps;
        public float timer, rotation;
        public Color color = Color.White;
        public bool isLooping = false, isVisible = true, isActive = true;
        public Vector2 origin = Vector2.Zero;
        public Vector2 scale = Vector2.One;
        public SpriteEffects spriteEffects;

        /**
         * Example texture name : "explosion_f17w64h64c6r3"
         * f = number of frames used
         * w = width of every frame
         * h = height of every frame
         * c = number of columns in spritesheet
         * r = number of rows in spritesheet
         * 
         * Exemple of animatedSprite instanciation :
         * Texture2D texture = Content.load<Texture2D>("path/to/my/spritesheet");
         * 
         * anim = new AnimatedSprite(texture, 10).animParam(true).centerAnimation();
        **/
        public AnimatedSprite(Texture2D texture, int fps) {
            this.texture = texture;
            this.fps = fps;
        }

        public Texture2D texture {
            get { return _texture; }
            set {
                _texture = value;
                string fileName = value.Name;                  //"particles / explosion1_f17w64h64c6r3"
                string textureParams = fileName.Split('_')[1]; //"f17w64h64c6r3"

                ParseAndSetParams(textureParams);
            }
        }

        public void Update(float dt) {
            if (!isActive) return;
            timer -= dt;
            if (timer > 0) return;

            timer = 1f / fps;
            currentFrame++;
            if (currentFrame >= totalFrames) {
                if (isLooping) {
                    currentFrame = 0;
                } else {
                    currentFrame--;
                    isActive = false;
                }
            }
            CalculateCurrentFramePosition();
        }

        public void CalculateCurrentFramePosition() {
            int row = currentFrame / cols;
            int col = currentFrame % cols;

            sourceRectangle.X = col * frameWth;
            sourceRectangle.Y = row * frameHgt;
        }

        //Example of unparsed params string: "f17w64h64c6r3"
        void ParseAndSetParams(string unparsedParams) {
            MatchCollection matches = Regex.Matches(unparsedParams, @"[a-zA-Z]+");
            int paramValue, paramLength;

            for (int i = 0; i < matches.Count; i++) {

                paramLength = i == matches.Count - 1 ? unparsedParams.Length - matches[i].Index - 1 : matches[i + 1].Index - 1 - matches[i].Index;
                paramValue = int.Parse(unparsedParams.Substring(matches[i].Index + 1, paramLength));

                switch (matches[i].Value) {
                    case "f": totalFrames = paramValue; break;
                    case "w": frameWth = paramValue; break;
                    case "h": frameHgt = paramValue; break;
                    case "c": cols = paramValue; break;
                    case "r": rows = paramValue; break;
                }
            }

            sourceRectangle.Width = frameWth;
            sourceRectangle.Height = frameHgt;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2? position, Color? otherColor = null, SpriteEffects? spriteEffects = null, float layerDepth = 0) {
            if (isVisible) {
                spriteBatch.Draw(texture, position ?? Vector2.Zero, sourceRectangle, otherColor ?? color, rotation, origin, scale, spriteEffects ?? this.spriteEffects, layerDepth);
            }
        }
    }
}
